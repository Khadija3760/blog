<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('welcome');
});

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/author', function () {
    return view('author');
});

Route::get('/blank', function () {
    return view('blank');
});

Route::get('/blog-post', function () {
    return view('blog-post');
});

Route::get('/catagory', function () {
    return view('catagory');
});

Route::get('/index', function () {
    return view('index');
});


